package progettoMultiThread;

import java.io.File;
import java.io.FileWriter; // Import the FileWriter class
import java.io.IOException; // Import the IOException class to handle errors

public class Tabellina extends Thread implements Runnable {

	private String nome_file;
	private int var_tab;

	public int getVar_tab() {
		return var_tab;
	}

	public void setVar_tab(int var_tab) {
		this.var_tab = var_tab;
	}

	public Tabellina(Integer valore) {
		super();
		String nome_file = valore.toString();
		this.nome_file =File.separator+"Tabellina_" + nome_file + ".txt";
		this.var_tab = valore;
		this.start();
	}

	public String getNome_file() {
		return nome_file;
	}

	public void setNome_file(String nome_file) {
		this.nome_file = nome_file;

	}
	
	@Override
	public void run() {
		try {
			int cont = 0;
			int valore = 0;
			String path = System.getProperty("user.dir");
			// TODO usa file.separator
			
			
			FileWriter myWriter = new FileWriter(path+this.getNome_file(),false);
			while (cont < 10) {
				cont++;
				valore += this.getVar_tab();
				myWriter.write(valore + "\n");
				myWriter.flush();
			}

			
			myWriter.close();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

}
