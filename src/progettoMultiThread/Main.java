package progettoMultiThread;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner interceptor = new Scanner(System.in);
		ArrayList<Integer> elenco_tab = new ArrayList<Integer>();

		System.out.println("Quante tabelline vuoi stampare?");
		int lim = interceptor.nextInt();
		int i = 0;

		while (i < lim) {
			i++;
			System.out.println("Tabellina numero " + i + " da stampare : ");
			elenco_tab.add(interceptor.nextInt());
		}

		// Verifica che le tabelline siano presenti nell'arraylist
		for (int j = 0; j < elenco_tab.size(); j++) {
			Integer temp = elenco_tab.get(j);
			Tabellina tab = new Tabellina(temp);
		}
	}

	// fai partire i thread e crea la classe tabelline

}
