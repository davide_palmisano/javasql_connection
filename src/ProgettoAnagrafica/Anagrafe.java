package ProgettoAnagrafica;

import java.util.ArrayList;

public class Anagrafe {
	private ArrayList<RigaInput> lista = new ArrayList<RigaInput>();
	
	public Anagrafe() {
		super();
	}

	public Anagrafe(ArrayList<RigaInput> lista) {
		super();
		this.lista = lista;
	}

	public ArrayList<RigaInput> getLista() {
		return lista;
	}

	public void setLista(ArrayList<RigaInput> lista) {
		this.lista = lista;
	}
	
	public void aggiungiInElenco(RigaInput obj_riga) {
		lista.add(obj_riga);
	}
	public void ricerca(String var_cogn) {
		for (int i = 0; i < getLista().size(); i++) {
			String cognome = getLista().get(i).getCognome();
			if(cognome.toLowerCase().contains(var_cogn.toLowerCase())) {
				System.out.println(getLista().get(i).toString());
			}
				
		}
	}
}

