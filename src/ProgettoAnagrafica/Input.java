package ProgettoAnagrafica;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Input {
	private String nome_campo;
	private int num_char ;
	
	
	
	public Input() {
		super();
	}

	private Input(String nome_campo, int num_char) {
		super();
		this.nome_campo = nome_campo;
		this.num_char = num_char;
	}
	
	public  Input verificaInput(String nome_campo, int num_char) {

		// TODO personalizza inserimento ; 
		
		if (nome_campo.length() <= num_char && !(nome_campo.isBlank())) {
			nome_campo = this.spaziatura(nome_campo, num_char);
			Input temp = new Input(nome_campo,num_char);
			return temp;
		}
		else {
			System.out.println("Numero limite di caratteri (pari a :  "+ num_char + "), superato!");
		}
		return null;

	}
	
	private String spaziatura(String nome_campo, int num_char) {
		String campo_tab = nome_campo;
		for (int i = 0; i < (num_char - nome_campo.length()); i++) {
			campo_tab += " ";
		}
		campo_tab += "\t";
		return campo_tab;
	}
	


	public int getNum_char() {
		return num_char;
	}
	public void setNum_char(int num_char) {
		this.num_char = num_char;
	}
	public String getNome_campo() {
		return nome_campo;
	}
	public void setNome_campo(String nome_campo) {
		this.nome_campo = nome_campo;
	}
	
	public  boolean isValidFormat(String value) {
		String format = "dd-mm-yyyy" ;
	    Date date = null;
	    try {
	        SimpleDateFormat sdf = new SimpleDateFormat(format);
	        date = sdf.parse(value);
	        if (!value.equals(sdf.format(date))) {
	        	System.out.println("Data inserita nel formato errato! \nInserire nel formato gg-mm-aaaa");
	            return false;
	        }
	    } catch (ParseException ex) {
	        ex.printStackTrace();
	        return false;
	    }
	    return true;
	}
	
}
