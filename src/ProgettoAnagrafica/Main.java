package ProgettoAnagrafica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Anagrafe elenco = new Anagrafe();
		File file_anagrafica;
		FileReader fr = null;

		Scanner interceptor = new Scanner(System.in);
		String[] array_input = { "nome", "cognome", "data di nascita", "luogo di nascita" };

		HashMap<String, Integer> hm = new HashMap<String, Integer>();

		String workingDir = System.getProperty("user.dir");
		FileWriter anagrafica = null;
		try {
			anagrafica = new FileWriter(workingDir + File.separator + "anagrafica.txt", true);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		hm.put("nome", 30);
		hm.put("cognome", 30);
		hm.put("data di nascita", 10);
		hm.put("luogo di nascita", 40);
		boolean continua = true;
		boolean continua2 = true;

		while (continua) {
			System.out.println("A - Accedi alla funzione scrittura\n"
					+ "R- Accedi alla funzione lettura \nT - Termina il programma");
			String scelta = interceptor.nextLine();
			switch (scelta) {
			case "T":
				continua = false;
				break;
			case "A":
				try {
					anagrafica = new FileWriter(workingDir + File.separator + "anagrafica.txt", true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				continua2 = true;
				while (continua2) {
					System.out.println("\t 1 - Inserisci campo\n" + "\t 0 - termina inserimento");
					String sotto_scelta = interceptor.nextLine();

					switch (sotto_scelta) {
					case "1":
						String riga_input = "";
						int i = 0;
						while (i < array_input.length) {
							boolean dataOK = true;
							System.out.println("Inserisci il campo: " + array_input[i]);
							String input_singolo = interceptor.nextLine();
							Input temp = new Input();
							if (array_input[i].equals("data di nascita")) {
								dataOK = temp.isValidFormat(input_singolo);
							}
							temp = temp.verificaInput(input_singolo, hm.get(array_input[i]));
							if (temp != null && dataOK) {
								i++;
								riga_input += temp.getNome_campo();
							}
						}

						try {
							anagrafica.write(riga_input + "\n");
							System.out.println("Inserimento avvenuto con successo!");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "0":
						continua2 = false;
						anagrafica.close();
						break;

					default:
						System.out.println("Errore nella scelta!");
						break;
					}
				}

				break;
			case "R":
				continua2 = true;
				file_anagrafica = new File(System.getProperty("user.dir") + File.separator + "anagrafica.txt");
				fr = new FileReader(file_anagrafica);

				BufferedReader bfr = new BufferedReader(fr);

				String line = null;
				while ((line = bfr.readLine()) != null) {
					String[] array_campi = line.split("\t");
					RigaInput riga = new RigaInput();
					// TODO SVUOTA PRIMA DI RIAGGIORNARE
					elenco.aggiungiInElenco(riga.generaRiga(array_campi));
				}
				while (continua2) {
					System.out.println("\t 1 - Stampa lista \n \t 2 - Ricerca per cognome \n \t 0 - Esci da ricerca");
					String sotto_scelta = interceptor.nextLine();
					switch (sotto_scelta) {
					case "1":
						for (int j = 0; j < elenco.getLista().size(); j++) {
							System.out.println(elenco.getLista().get(j).toString());
						}
						break;

					case "2":
						System.out.println("Inserisci cognome da ricercare");
						String var_cogn = interceptor.nextLine();
						elenco.ricerca(var_cogn);
						break;
					case "0":
						continua2 = false;
						fr.close();
						break;
					default:
						break;
					}

				}
			}
		}

		interceptor.close();

	}

}
