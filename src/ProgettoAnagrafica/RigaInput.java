package ProgettoAnagrafica;

public class RigaInput {
	private String nome;
	private String cognome;
	private String data_nascita;
	private String luogo_nascita;

	public RigaInput() {
		super();
	}

	@Override
	public String toString() {
		return nome + "\t" + cognome + "\t" + data_nascita + "\t" + luogo_nascita;
	}

	private RigaInput(String nome, String cognome, String data_nascita, String luogo_nascita) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.data_nascita = data_nascita;
		this.luogo_nascita = luogo_nascita;
	}

	public RigaInput generaRiga(String[] array_campi) {
		RigaInput riga = new RigaInput(array_campi[0], array_campi[1], array_campi[2], array_campi[3]);
		return riga;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getData_nascita() {
		return data_nascita;
	}

	public void setData_nascita(String data_nascita) {
		this.data_nascita = data_nascita;
	}

	public String getLuogo_nascita() {
		return luogo_nascita;
	}

	public void setLuogo_nascita(String luogo_nascita) {
		this.luogo_nascita = luogo_nascita;
	}

}
