package threads_prova;

public class Operatore extends Thread implements Runnable {
	private int contatore;

	public Operatore(int contatore) {
		super();
		this.contatore = contatore;
	}

	public int getContatore() {
		return contatore;
	}

	public void setContatore(int contatore) {
		this.contatore = contatore;
	}

	@Override
	public void run() {
		System.out.println("run --> " + this.getContatore());

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Fine operazione");
	}
}
