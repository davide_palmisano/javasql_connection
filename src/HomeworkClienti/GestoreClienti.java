package HomeworkClienti;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class GestoreClienti {
	private ArrayList<Cliente> elenco = new ArrayList<Cliente>();
	private String path = System.getProperty("user.home") + "\\Desktop\\Java\\";

	public GestoreClienti() {
		super();
		try {
			this.select();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void select() throws SQLException {
		Connection connessione = ConnettoreDB.getIstanza().getConnessione();
		String query = " SELECT clienteId,nome,cognome,telefono,cod_cli FROM cliente";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ResultSet risultato = ps.executeQuery();

		while (risultato.next()) {
			Cliente cli_temp = new Cliente();
			cli_temp.setId(risultato.getInt(1));
			cli_temp.setNome(risultato.getString(2));
			cli_temp.setCognome(risultato.getString(3));
			cli_temp.setTelefono(risultato.getString(4));
			cli_temp.setCod_cli(risultato.getString(5));
			cli_temp.setCod_cli(generaCodice(cli_temp.getId()));
			elenco.add(cli_temp);
			System.out.println(cli_temp.toString());
		}
	}

	public int delete(String codice) throws SQLException {
		Connection connessione = ConnettoreDB.getIstanza().getConnessione();
		String query = " DELETE FROM cliente WHERE cod_cli = ?";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ps.setString(1, codice);
		int risultato_operazione = ps.executeUpdate();
		if (risultato_operazione > 0) {
			System.out.println("Riga eliminata!");
		} else {
			System.out.println("Errore di eliminazione");
		}
		return risultato_operazione;
	}

	public void insert(Cliente obj_cliente) throws SQLException {
		Connection connessione = ConnettoreDB.getIstanza().getConnessione();
		String query = " INSERT INTO cliente (nome,cognome,telefono) VALUES (?,?,?)";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

		ps.setString(1, obj_cliente.getNome());
		ps.setString(2, obj_cliente.getCognome());
		ps.setString(3, obj_cliente.getTelefono());

		ps.executeUpdate(); // Eseguo l'inserimento
		ResultSet risultato = ps.getGeneratedKeys(); // Prendo il risultato che � l'indice (AI) della riga appena creata
		risultato.next();

		int id_risultante = risultato.getInt(1);
		obj_cliente.setCod_cli(generaCodice(id_risultante));
		obj_cliente.setId(id_risultante);
		elenco.add(obj_cliente);
	}

	public String generaCodice(Integer id) throws SQLException {
		String codice = id.toString();
		for (int i = 0; i < 3 - id.toString().length(); i++) {
			codice = "0" + codice;
		}
		codice = "CLI" + codice;

		Connection connessione = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE cliente SET cod_cli = ? WHERE clienteId = ?";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);

		ps.setString(1, codice);
		ps.setInt(2, id);
		ps.executeUpdate();
		return codice;
	}

	public void scriviFile() throws IOException {
		pulisciFile();
		File d = new File(path);
		if (d.exists() && d.isDirectory()) {
			File f = new File(path + "elencoClienti.txt");
			FileWriter fw = new FileWriter(f, true);
			fw.write(stampaTitolo());
			for (int i = 0; i < this.elenco.size(); i++) {
				Cliente temp = new Cliente();
				temp = this.elenco.get(i);
				String line = spaziatura(temp);
				fw.write(line + "\n");

			}
			fw.close();
		}
	}

	public String stampaTitolo() {

		String nome = String.format("%-20s", "Nome");
		String cognome = String.format("%-20s", "Cognome");
		String telefono = String.format("%-20s", "Telefono");
		String codice = String.format("%-20s", "Codice Cliente");

		String line = nome + cognome + telefono + codice + "\n\n";

		return line;

	}

	public String spaziatura(Cliente obj_cliente) {

		String nome = String.format("%-20s", obj_cliente.getNome());
		String cognome = String.format("%-20s", obj_cliente.getCognome());
		String telefono = String.format("%-20s", obj_cliente.getTelefono());
		String codice = String.format("%-20s", obj_cliente.getCod_cli());

		String line = nome + cognome + telefono + codice;

		return line;

	}

	public void pulisciFile() {
		String nomeFile = "elencoClienti.txt";
		File f = new File(path + nomeFile);
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.print("");
		writer.close();
	}

	public void fileToDb() throws FileNotFoundException, SQLException {
		String nome_file = "MOCK_DATA.csv";
		pulisciDB();
		File f = new File(path + nome_file);
		Scanner scan = new Scanner(f);

		while (scan.hasNext()) {
			String riga = scan.nextLine();
			String[] array = riga.split(",");
			Cliente temp = new Cliente(array[0], array[1], array[2]);
			if(verificaEsistenza(temp) == 0) {
			insert(temp);
			}
		}
		scan.close();
	}
	
	public void pulisciDB() throws SQLException{
		ArrayList<Integer> idList =  getId();
		for (int j = 0; j < idList.size(); j++) {
			Integer id = idList.get(j);
			String codice = id.toString();
			for (int i = 0; i < 3 - id.toString().length(); i++) {
				codice = "0" + codice;
			}
			codice = "CLI" + codice;
			
			try {
				delete(codice);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
}
	
	public int verificaEsistenza(Cliente obj_cli) throws SQLException{
		
		Connection connessione = ConnettoreDB.getIstanza().getConnessione();
		String query = " SELECT nome,cognome,telefono FROM cliente WHERE nome = ? && cognome = ? && telefono = ?";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ps.setString(1, obj_cli.getNome());
		ps.setString(2, obj_cli.getCognome());
		ps.setString(3, obj_cli.getTelefono());
		ResultSet risultato = ps.executeQuery();
		int cont = 0 ; 
		while (risultato.next()) {
			cont++;
		}
		
		return cont;
	}
	
	public ArrayList<Integer> getId() throws SQLException {
		ArrayList<Integer> id = new ArrayList<Integer>();
		Connection connessione = ConnettoreDB.getIstanza().getConnessione();
		String query = " SELECT clienteId FROM cliente ";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			id.add(risultato.getInt(1));
		}
			
		return id ; 
	}

}
