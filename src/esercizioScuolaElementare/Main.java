package esercizioScuolaElementare;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	

	public static void main(String[] args) {
		String nuovaCartella = "\\scuolaElementare";
		Scanner scan = new Scanner(System.in);
		String filePath = System.getProperty("user.home") + "\\Desktop\\Java";
		File d = new File(filePath);
		if (d.exists() && d.isDirectory()) {
			File nd = new File(filePath + nuovaCartella);
			if (!nd.exists()) {
				nd.mkdir();
			} else {
			}

		}
		String[] array_campi = { "Nome", "Cognome", "Classe", "Sezione" };
		ElencoStudenti elenco = new ElencoStudenti();
		ManageFile file = new ManageFile();

		boolean continua = true;

		while (continua) {
			System.out.println("1 - Inserisci studente\n2 - Stampa tutti gli studenti\n0 - Termina");
			String opzione = scan.nextLine();
			switch (opzione) {
			case "1":
				int i = 0;
				String[] riga_input = { "", "", "", "" };
				while (i < 4) {
					System.out.println("Inserisci : " + array_campi[i]);
					riga_input[i] = scan.nextLine();
					i++;
				}
				Studente nuovo_stud = new Studente();
				if (nuovo_stud.aggiungiStudente(riga_input)) {
						elenco.aggiungiStudente(nuovo_stud);
						System.out.println("Studente aggiunto con successo!");
						try {
							file.scriviFile(nuovo_stud);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

				}

				break;
			case "2":
				file.gestioneFile();
				break;
			case "0":
				continua = false;
				break;

			default:
				break;
			}
		}

	}
}
