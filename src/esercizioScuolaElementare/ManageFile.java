package esercizioScuolaElementare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

public class ManageFile {


	public void scriviFile(Studente obj_studente) throws IOException {
		String var_path = creaCartella(obj_studente);
		String nomeFile = "\\" + obj_studente.getClasse() + ".txt";
		FileWriter fw = null;
		File fileScuola = new File(var_path + nomeFile);
		try {
			fw = new FileWriter(fileScuola, true);
			fw.write(obj_studente.getNome() + "\t" + obj_studente.getCognome() + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fw.close();
	}

	public String creaCartella(Studente obj_studente) {
		String nuovaCartella = "\\" + obj_studente.getSezione();
		String filePath = System.getProperty("user.home") + "\\Desktop\\Java\\scuolaElementare";

		File d = new File(filePath);
		if (d.exists() && d.isDirectory()) {
			File nd = new File(filePath + nuovaCartella);
			if (!nd.exists()) {
				nd.mkdir();
			} else {
			}

		}
		return filePath + nuovaCartella;

	}

	public void gestioneFile() {
		// Lista dei file all'interno di una directory
		int contatore = 0 ;
		pulisciFile();
		String filePath = System.getProperty("user.home") + "\\Desktop\\Java\\scuolaElementare";
		File d = new File(filePath);
		ArrayList<String> elenco = new ArrayList<String>();
		if (d.exists() && d.isDirectory()) {
			File[] lista_dir = d.listFiles();
			for (int i = 0; i < lista_dir.length; i++) {
				String nome_dir = lista_dir[i].getName();
				if (lista_dir[i].isDirectory()) {
					File text = new File(filePath + File.separator + nome_dir);
					File[] lista_file = text.listFiles();
					for (int j = 0; j < lista_file.length; j++) {						
						elenco.clear();
						elenco = leggiFile(lista_file[j]);
						contatore = stampaElenco(elenco, lista_file[j].getName(), lista_dir[i].getName(), contatore);
					}

				}
			}
		}
	}

	public ArrayList<String> leggiFile(File f) {
		Scanner scan = null;
		ArrayList<String> elenco = new ArrayList<String>();
		try {
			scan = new Scanner(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		while (scan.hasNext()) {
			String riga = scan.nextLine();
			elenco.add(riga);
		}
		scan.close();
		return elenco;
	}

	public int stampaElenco(ArrayList<String> elenco, String var_file, String var_dir, int contatore) {
		String filePath = System.getProperty("user.home") + "\\Desktop\\Java\\";
		String nomeFile = "elenco_completo.txt";
		FileWriter fw = null;
		File f = new File(filePath + nomeFile);
		elenco = ordinaElenco(elenco);
		try {
			fw = new FileWriter(f, true);
			for (int i = 0; i < elenco.size(); i++) {
				String riga = elenco.get(i);
				String[] riga_split = riga.split("\t");
				contatore++;
				fw.write(contatore+". "+riga_split[0] + "\t" + riga_split[1] + " - " + var_file.substring(0,1) + var_dir+"\n");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contatore;
	}
	
	public void pulisciFile() {
		String filePath = System.getProperty("user.home") + "\\Desktop\\Java\\";
		String nomeFile = "elenco_completo.txt";
		File f = new File(filePath + nomeFile);
		
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.print("");
		writer.close();
	}
	
	public ArrayList<String> ordinaElenco(ArrayList<String> elenco) {
		Collections.sort(elenco);
		return elenco;
	}
}