package esercizioScuolaElementare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Studente {
	private String nome;
	private String cognome;
	private String classe;
	private String sezione;

	public Studente() {
		super();
	}

	public boolean aggiungiStudente(String[] input) {
		boolean ok = false;
		if (checkClasse(input[2])) {
			if (checkSezione(input[3])) {
				this.nome = input[0];
				this.cognome = input[1];
				this.classe = input[2];
				this.sezione = input[3];
				ok = true;
			}
		}
		return ok;
	}

	public String getNome() {
		return nome;
	}

	public String getSezione() {
		return sezione;
	}

	public void setSezione(String sezione) {
		this.sezione = sezione;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public boolean checkClasse(String var_classe) {
		boolean check = false;
		int numero = Integer.parseInt(var_classe);
		if (numero > 0 && numero <= 5) {
			check = true;
		} else {
			System.out.println("Classe non esistente!");
		}
		return check;
	}

	public boolean checkSezione(String var_sezione) {
		boolean check = false;
		String[] array_sezioni = { "A", "B", "C", "D", "E", "F" };
		for (int i = 0; i < array_sezioni.length; i++) {
			if (var_sezione.toUpperCase().equals(array_sezioni[i])) {
				check = true;
			}

		}
		if (!check) {
			System.out.println("Sezione non presente!");
		}
		return check;
	}
	
	public boolean readFile(Studente obj_studente) {
		boolean trovato = false;
		String filePath = System.getProperty("user.home") + "\\Desktop\\Java\\scuolaElementare" + "\\"
				+ obj_studente.getSezione()+"\\"+obj_studente.getClasse();
		File f = new File(filePath);
		if (f.exists() && f.isFile() && f.canRead()) {
			try {
				FileReader fr = new FileReader(f);
				BufferedReader buff = new BufferedReader(fr);
				String var_temp;

				while ((var_temp = buff.readLine()) != null) {
					String[] array_campi = var_temp.split("\t");
					trovato = trovaStudente(obj_studente.getSezione(),obj_studente.getClasse(),array_campi);
				}

				fr.close();

			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return trovato ;
	}


}
