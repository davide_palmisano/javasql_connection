package esercizioScuolaElementare;

import java.util.ArrayList;

public class ElencoStudenti {
	private ArrayList<Studente> elenco = new ArrayList<Studente>();

	public ArrayList<Studente> getElenco() {
		return elenco;
	}

	public void setElenco(ArrayList<Studente> elenco) {
		this.elenco = elenco;
	}

	public ElencoStudenti() {
		super();
	}

	public void aggiungiStudente(Studente obj_studente) {
		this.getElenco().add(obj_studente);
	}
	
	
}
