package lezione19.prima_prova;
import java.sql.Connection;
import java.sql.ResultSet;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Main {

	public static void main(String[] args) {


		ElencoStudenti elenco = new ElencoStudenti();

		try {

			Connection conn = dataSource.getConnection();

			String query = "SELECT StudenteID,nome,cognome,matricola "
					+ "FROM studente";
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
			ResultSet risultato = ps.executeQuery();

			while(risultato.next()) {
				int var_id = risultato.getInt(1);
				String var_nome = risultato.getString(2);
				String var_cognome = risultato.getString(2);
				String var_matricola = risultato.getString(2);

				elenco.inserisciStudente(var_id, var_nome, var_cognome, var_matricola);

			}

			elenco.stampaDettagli();
			
			
			
			conn.close();
			
			Gestore gestore = new Gestore();
			
			gestore.inserisciStudente(dataSource, "Simone", "Leporati", "2648336");
			
			
			
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}


	}


}


