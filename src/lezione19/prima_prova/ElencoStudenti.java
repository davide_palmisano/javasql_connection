package lezione19.prima_prova;
import java.util.ArrayList;


public class ElencoStudenti {
	private ArrayList<Studente> elenco = new ArrayList<Studente>();
	
	public void inserisciStudente(
			int var_id,
			String var_nome,
			String var_cognome,
			String var_matricola) {
		
		Studente stud_temp = new Studente(
				var_id,
				var_nome,
				var_cognome,
				var_matricola);
		elenco.add(stud_temp);
		}
	
	public void stampaDettagli(){
		for (int i = 0; i < elenco.size(); i++) {
			Studente stud_temp = elenco.get(i);
			System.out.println(stud_temp.toString());
		}
	}
	


}
