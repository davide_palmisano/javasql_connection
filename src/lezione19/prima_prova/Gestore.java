package lezione19.prima_prova;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;




public class Gestore {
	private ArrayList<Studente> elenco = new ArrayList<Studente>();
	private MysqlDataSource dataSource;
	
	Gestore(){
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setServerName("127.0.0.1");
		dataSource.setPortNumber(3306);
		dataSource.setUser("root");
		dataSource.setPassword("toor");
		dataSource.setUseSSL(false);
		dataSource.setDatabaseName("lez_2_universita");
		
		this.select();
	}
		
	private void select() {	
		Connection conn = this.dataSource.getConnection();
		
		String query = "SELECT PersonaID, nome, cognome, matricola  FROM studente;";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			int var_id =  risultato.getInt(1);
			String var_nome = risultato.getString(2);
			String var_cognome = risultato.getString(3);
			String var_matricola = risultato.getString(4);
			
			Studente temp = new Studente();
			temp.setId(var_id);
			temp.setNome(var_nome);
			temp.setCognome(var_cognome);
			temp.setMatricola(var_matricola);
			this.elenco.add(temp);
	}
	
	public void inserisciStudente(
			String var_nome,
			String var_cognome,
			String var_matricola
			) {

	}

	private void insert() {

	}
		
}
