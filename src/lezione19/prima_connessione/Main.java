package lezione19.prima_connessione;


import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Main {

	public static void main(String[] args) {
		
		MysqlDataSource dataSource = new MysqlDataSource() ;
		// Istruzioni per connettersi
		dataSource.setServerName("127.0.0.1");
		dataSource.setPortNumber(3306);
		dataSource.setUser("root");
		dataSource.setPassword("toor");
		dataSource.setUseSSL(false);
		dataSource.setDatabaseName("lez_19_rubrica");
		
		// Creazione connessione
		
		ArrayList<Persona> elenco = new ArrayList<Persona>();
		
		
		try {
			Connection conn = dataSource.getConnection();
			
			// Spedisco la query mettendol nel pacchetto ps
			String query = "SELECT PersonaID, Nome, Cognome, Telefono "
					+ "FROM Persona";
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
			
			// Ricevo la query e la metto in un Result Set
			ResultSet risultato = ps.executeQuery();
			// Scandisco la lista finche ogni elemento ha un successivo
			while(risultato.next()){
				int persona_id = risultato.getInt(1);
				String nome = risultato.getString(2);
				String cognome = risultato.getString(3);
				String telefono = risultato.getString(4);
				
				Persona pers_temp = new Persona();
				pers_temp.setCognome(cognome);
				pers_temp.setId(persona_id);
				pers_temp.setNome(cognome);
				pers_temp.setTelefono(telefono);
				elenco.add(pers_temp);
			}
			conn.close();
			
			for (int i = 0; i < elenco.size(); i++) {
				System.out.println(elenco.get(i).toString());
			}
			
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		
		
	}

}
