package lezione19.classi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Rubrica {
	private ArrayList<Persona> elenco = new ArrayList<Persona>();
	private MysqlDataSource dataSource;
	
	
	Rubrica(){
		
		this.dataSource = new MysqlDataSource();
		this.dataSource.setServerName("127.0.0.1");  //equivale a localhost
		this.dataSource.setPortNumber(3306);
		this.dataSource.setUser("root");
		this.dataSource.setPassword("toor");
		this.dataSource.setUseSSL(false);//settiamo in modo che possa comunicare anche in http senza ssl 
		this.dataSource.setDatabaseName("lez_19_rubrica");
		
		this.select();
	}
	
	private void select() {
		
		try {
			
			this.elenco.clear();
			
			Connection conn = this.dataSource.getConnection();
			
			String query = "SELECT PersonaID, Nome, Cognome,Telefono FROM Persona;";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
			
			ResultSet risultato = ps.executeQuery();
			while(risultato.next()) {
				int var_id =  risultato.getInt(1);
				String var_nome = risultato.getString(2);
				String var_cognome = risultato.getString(3);
				String var_telefono = risultato.getString(4);
				
				Persona temp = new Persona();
				temp.setId(var_id);
				temp.setNome(var_nome);
				temp.setCognome(var_cognome);
				temp.setTelefono(var_telefono);
				this.elenco.add(temp);
			}
			
			conn.close();
			
		} catch (Exception e) {
			System.out.println("Errore di reperimento dati!");
		}
		
	}
	
	public boolean inserisciNuovaPersona(String var_nome, String var_cognome, String var_telefono) {
		Persona nuova = new Persona();
		nuova.setNome(var_nome);
		nuova.setCognome(var_cognome);
		nuova.setTelefono(var_telefono);

		insert(nuova);
		
		if(nuova.getId() > 0) {
			elenco.add(nuova);		//Poco sicuro
			return true;
		}
		return false;
	}
	
	private Persona insert(Persona obj_persona) {
		try {
			Connection conn = dataSource.getConnection();
			
			String query_inserimento = "INSERT INTO Persona(Nome, Cognome, Email, Telefono) VALUE (?, ?, ?)";
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_inserimento, Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, obj_persona.getNome());
			ps.setString(2, obj_persona.getCognome());
			ps.setString(3, obj_persona.getTelefono());
			
			ps.executeUpdate();								//Eseguo l'inserimento
			ResultSet risultato = ps.getGeneratedKeys();	//Prendo il risultato che � l'indice (AI) della riga appena creata
			risultato.next();
			
			int id_risultante = risultato.getInt(1);
			obj_persona.setId(id_risultante);
			
			conn.close();
			return obj_persona;
		} catch (SQLException e) {
			return null;
		}
	}
	
	public void delete(int var_id) {
      try {
      Connection conn = dataSource.getConnection();

      String query_delete = "DELETE FROM Persona WHERE PersonaID = ?";
      PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_delete);
      ps.setInt(1, var_id);                                        //Elimino la Persona con ID 4

      int risultato_operazione = ps.executeUpdate();
      if(risultato_operazione > 0) {
          System.out.println("Riga eliminata!");
      }
      else {
          System.out.println("Errore di eliminazione");
      }


  } catch (SQLException e) {
      System.out.println(e.getMessage());
  }
	}
	
	
	public void stampaElenco() {
		this.select();
		
		for(int i=0; i<this.elenco.size(); i++) {
			Persona temp = this.elenco.get(i);
			System.out.println(temp.toString());
		}
	}
}
