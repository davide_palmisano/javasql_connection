package HomeworkLibreria;

import java.util.ArrayList;

public class Iscritto {
	private int id;
	private String nome;
	private String cognome;
	private String cod_fis;
	private String tes_cli;
	private ArrayList<Prestito> elenco_pres ; 
	
	
	public Iscritto(){
		super();
	}
	
	public Iscritto(String nome, String cognome, String cod_fis) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.cod_fis = cod_fis;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getCod_fis() {
		return cod_fis;
	}
	public void setCod_fis(String cod_fis) {
		this.cod_fis = cod_fis;
	}
	public String getTes_cli() {
		return tes_cli;
	}
	public void setTes_cli(String tes_cli) {
		this.tes_cli = tes_cli;
	}
	
	
}
