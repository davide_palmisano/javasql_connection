package HomeworkLibreria;

import java.util.ArrayList;

public class Prestito {
	private int id ;
	private String data_acc;
	private String data_rie;
	private String codice;
	private ArrayList<Libro> elenco_lib ;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getData_acc() {
		return data_acc;
	}
	public void setData_acc(String data_acc) {
		this.data_acc = data_acc;
	}
	public String getData_rie() {
		return data_rie;
	}
	public void setData_rie(String data_rie) {
		this.data_rie = data_rie;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public ArrayList<Libro> getElenco_lib() {
		return elenco_lib;
	}
	public void setElenco_lib(ArrayList<Libro> elenco_lib) {
		this.elenco_lib = elenco_lib;
	} 
	
	

	
}
