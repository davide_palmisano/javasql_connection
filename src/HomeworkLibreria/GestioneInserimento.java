package HomeworkLibreria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.text.DateFormatter;

import HomeworkClienti.ConnettoreDB;

/**
 * Costruttore del gestore di biblioteca: ha una funzione che assegna il codice
 * iscritto ai dati dummy gi� presenti nel database
 * 
 * @author PalmisanoDa
 *
 */
public class GestioneInserimento {
	public GestioneInserimento() {
		try {
			ArrayList<Iscritto> elenco = selectIscritti();
			for (int i = 0; i < elenco.size(); i++) {
				Iscritto temp = elenco.get(i);
				String codice = generaCodice(temp);
				updateCodice(codice, temp.getId());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Inserisce il libro, gli autori e le categorie associate al libro che non sono gi� presenti nel db
	 * e popola le tabelle di appoggio.
	 * @param titolo
	 * @param codice
	 * @param elenco_aut
	 * @param elenco_cat
	 * @throws SQLException
	 */
	public void inserisciLibro(String titolo, String codice, ArrayList<Autore> elenco_aut,
			ArrayList<Categoria> elenco_cat) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		// TODO CONROLLO ESISTENZA AUTORE E CATEGORIA, SE NON ESISTONO LI CREO
		ArrayList<Integer> id_autori = cercaAutore(elenco_aut);
		ArrayList<Integer> id_categ = cercaCategoria(elenco_cat);

		Libro temp = new Libro(titolo, codice, elenco_aut, elenco_cat);
		String query = "INSERT INTO libro (titolo,codice) VALUE (?,?)";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, titolo);
		ps.setString(2, codice);
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		int id_risultante = risultato.getInt(1);
		temp.setId(id_risultante);

		inserisciLibroAutore(id_risultante, id_autori);
		inserisciLibroCategoria(id_risultante, id_categ);
		System.out.println("Libro inserito con successo!");
	}

	public void inserisciIscritto(String var_nome, String var_cognome, String cod_fis) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		Iscritto temp = new Iscritto(var_nome, var_cognome, cod_fis);
		String query = "INSERT INTO iscritto (nome,cognome,cod_fis) VALUE (?,?,?)";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, var_nome);
		ps.setString(2, var_cognome);
		ps.setString(3, cod_fis);

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		int id_risultante = risultato.getInt(1);
		temp.setId(id_risultante);
		String tessera = generaCodice(temp);
		updateCodice(tessera, id_risultante);
		temp.setTes_cli(tessera);
		System.out.println("Iscritto inserito con successo!");

		// TODO AGGIUNGI A ELENCO

	}

	public void inserisciLibroAutore(int id, ArrayList<Integer> id_autori) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		for (int i = 0; i < id_autori.size(); i++) {
			int id_temp = id_autori.get(i);
			String query = "insert into libro_autore (idLibro, idAutore) values (?, ?);";
			PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
			ps.setInt(1, id);
			ps.setInt(2, id_temp);
			ps.executeUpdate();
		}
	}

	public void inserisciLibroCategoria(int id, ArrayList<Integer> id_categ) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		for (int i = 0; i < id_categ.size(); i++) {
			int id_temp = id_categ.get(i);
			String query = "insert into libro_categoria (idLibro, idCategoria) values (?, ?);";
			PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
			ps.setInt(1, id);
			ps.setInt(2, id_temp);
			ps.executeUpdate();
		}
	}

	public String generaCodice(Iscritto temp) {
		String codice = "";
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyy");
		LocalDateTime now = LocalDateTime.now();
		codice = temp.getNome().substring(0, 3).toUpperCase() + temp.getCognome().substring(0, 3).toUpperCase() + "-"
				+ dtf.format(now);
		return codice;

	}

	public void updateCodice(String codice, int id) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "UPDATE iscritto SET tes_id = ? WHERE id = ?";
		;
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ps.setString(1, codice);
		ps.setInt(2, id);
		ps.executeUpdate();
	}

	public ArrayList<Integer> cercaAutore(ArrayList<Autore> elenco_aut) throws SQLException {
		ArrayList<Integer> elenco_id = new ArrayList<Integer>();
		for (int i = 0; i < elenco_aut.size(); i++) {
			Autore temp = elenco_aut.get(i);
			Connection connessione = ConnessioneDB.getIstanza().getConnessione();
			String query = "SELECT id FROM autore WHERE nome = ? && cognome = ?";
			PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
			ps.setString(1, temp.getNome());
			ps.setString(2, temp.getCognome());
			ResultSet risultato = ps.executeQuery();
			if (!risultato.next()) {
				elenco_id.add(inserisciAutore(temp));
				System.out.println("Autore non presente nell'elenco ed inserito con successo!");
			} else {
				elenco_id.add(risultato.getInt(1));
			}
		}
		return elenco_id;
	}

	public ArrayList<Integer> cercaCategoria(ArrayList<Categoria> elenco_cat) throws SQLException {
		ArrayList<Integer> elenco_id = new ArrayList<Integer>();
		for (int i = 0; i < elenco_cat.size(); i++) {
			Categoria temp = elenco_cat.get(i);
			Connection connessione = ConnessioneDB.getIstanza().getConnessione();
			String query = "SELECT id FROM categoria WHERE nome = ?";
			PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
			ps.setString(1, temp.getNome());
			ResultSet risultato = ps.executeQuery();
			if (!risultato.next()) {
				elenco_id.add(inserisciCategoria(temp));
				System.out.println("Categoria non presente nell'elenco ed inserito con successo!");
			} else {
				elenco_id.add(risultato.getInt(1));
			}
		}
		return elenco_id;
	}

	public int inserisciAutore(Autore obj_aut) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "INSERT INTO autore (nome,cognome) VALUE (?,?)";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, obj_aut.getNome());
		ps.setString(2, obj_aut.getCognome());
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		int id_risultante = risultato.getInt(1);
		return id_risultante;

	}

	public int inserisciCategoria(Categoria obj_aut) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "INSERT INTO categoria (nome) VALUE (?)";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, obj_aut.getNome());
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		int id_risultante = risultato.getInt(1);
		return id_risultante;
	}

	public ArrayList<Iscritto> selectIscritti() throws SQLException {
		ArrayList<Iscritto> elenco = new ArrayList<Iscritto>();
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "SELECT nome,cognome,id from iscritto";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {
			Iscritto temp = new Iscritto();
			temp.setNome(risultato.getString(1));
			temp.setCognome(risultato.getString(2));
			temp.setId(risultato.getInt(3));
			elenco.add(temp);
		}
		return elenco;
	}
}
