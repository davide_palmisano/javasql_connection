package lezione20.Singleton;

public class Prodotto_Categoria {
	private String articolo;
	private String descrizione;
	
	public String getArticolo() {
		return articolo;
	}
	public void setArticolo(String articolo) {
		this.articolo = articolo;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	@Override
	public String toString() {
		return "Prodotto_Categoria [articolo=" + articolo + ", descrizione=" + descrizione + "]";
	}
	
	
}
