package lezione20.Singleton;

import java.util.ArrayList;

public class Prodotto {
	private String articolo;
	private ArrayList<String> categoria;
	private int id;

	public int getId() {
		return id;
	}

	public ArrayList<String> getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria.add(categoria);
	}

	public Prodotto(String articolo) {
		super();
		this.articolo = articolo;
	}

	public String getArticolo() {
		return articolo;
	}

	public void setArticolo(String articolo) {
		this.articolo = articolo;
	}

}
