package lezione20.Singleton;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnectorDB {
	private Connection conn;
	private static ConnectorDB obj_connessione;

	public static ConnectorDB createIstanza()throws SQLException{
		if (obj_connessione == null) {
			obj_connessione = new ConnectorDB();
			return obj_connessione;
		} else
			return obj_connessione;
	}

	public Connection createConnection() throws SQLException{
		if (this.conn == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("127.0.0.1");
			dataSource.setPortNumber(3306);
			dataSource.setUser("root");
			dataSource.setPassword("toor");
			dataSource.setUseSSL(false);
			dataSource.setDatabaseName("prodotti_categorie");

			Connection conn =  dataSource.getConnection();
			return conn;
		}
		else
			return conn;
	}

}
