package lezione20.Singleton;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Gestore {
	private ArrayList<Prodotto_Categoria> elenco = new ArrayList<Prodotto_Categoria>();

	public void insert(Prodotto obj_prodotto) throws SQLException {
		Prodotto temp = obj_prodotto;
		Connection connessione = ConnectorDB.createIstanza().createConnection();
		String query_inserimento = "INSERT INTO prodotto (articolo) VALUE (?)";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query_inserimento);
		ps.setString(1, obj_prodotto.getArticolo());
		int risultato = ps.executeUpdate();
		if (risultato > 0) {
			System.out.println("Operazione avvenuta con successo!");
		}

		for (int i = 0; i < temp.getCategoria().size(); i++) {

		}

	}

	public void select(int id_articolo, String var_categoria) throws SQLException {
		Connection connessione = ConnectorDB.createIstanza().createConnection();
		String query = "SELECT categoriaID,descrizione FROM categoria WHERE categoria.descrizione = ? ";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ps.setString(1, var_categoria);
		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {
			int var_id = risultato.getInt(1);

		}
	}

	public void join(Prodotto obj_prodotto) throws SQLException {

		Connection connessione = ConnectorDB.createIstanza().createConnection();

		String query_inserimento = "SELECT prodotto.articolo , categoria.descrizione " + "FROM prodotto "
				+ "JOIN categoria_prodotto ON prodotto.prodottoID = categoria_prodotto.prodotto_id "
				+ "JOIN categoria ON categoria_prodotto.categoria_id = categoria.categoriaID "
				+ "WHERE prodotto.articolo =  ? ";

		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query_inserimento);

		ps.setString(1, obj_prodotto.getArticolo());
		ResultSet risultato = ps.executeQuery();

		while (risultato.next()) {
			String var_articolo = risultato.getString(1);
			String var_categoria = risultato.getString(2);

			Prodotto_Categoria prod_cat_temp = new Prodotto_Categoria();
			prod_cat_temp.setArticolo(var_articolo);
			prod_cat_temp.setDescrizione(var_categoria);
			elenco.add(prod_cat_temp);

		}

		for (int i = 0; i < elenco.size(); i++) {
			System.out.println(elenco.get(i).toString());
		}

//		int id_risultante = risultato.getInt(1);
//		esa.setId(id_risultante);

	}
}
