package lezione20.Singleton;

import java.sql.SQLException;

public class Main {

	public static void main(String[] args) {

		Prodotto prod = new Prodotto("IPhone X");
		Prodotto prod_2 = new Prodotto("MACBOOK PRO");
		Gestore gest = new Gestore();

		try {
			gest.insert(prod_2);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			gest.join(prod);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
